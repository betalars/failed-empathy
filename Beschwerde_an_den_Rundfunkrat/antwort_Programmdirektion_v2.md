*Hier veröffentliche ich nun die Antwort des Intendanten, die ich gemäß eines geregelten Beschwerdeverfahren im zweiten Anlauf nun bekommen habe sowie meine Antwort darauf. Die Antwort des Intendanten ist hier nur als Transkript, weil es mir nur als analoges Dokuemnt und als Pixelgrafik vorliegt. Die exakte inhaltliche Korrektheit kann ich daher nicht gewährleisten.*

## Antwort des Intendanten

Sehr geehrter [betalars],

in Ihren E-Mails vom 10.12.2020 und 15.12.2020 an die Vorsitzende des Fernsehrates haben Sie unsere Sendungen Ella Schön - Die Inselbegabung und Ella Schon - Die nackte Wahrheit in der ZDFmediathek angesprochen. Die Fernsehratsvorsitzende hat Ihre Eingabe gemaß § 21 Abs. 2 der ZDF-Satzung (Beschwerdeardnung) an mich zur Prüfung weitergeleitet. Gerne möchte ich Ihnen hiermit antworten und Sie zugleich darüber informieren, dass die Fernsehratsvorsitzende eine Kopie dieses Schreibens zur Kenntnis erhält.

Sie monieren mangelnde Kenntnisse bezüglich Asperger Autismus seitens der produktionell Beteiligten, insbesondere der Hauptdarstellerin. Ich kann Ihnen versichern, dass sich Frau Frier, wie alle anderen Beteiligten der Produktion, sehr umfassend über Asperger Autismus informiert hat. Schlussendlich ist es Aufgabe der darstellenden Kunstlerin, begleitet von einer Regie, ihre erarbeitete Form der fiktiven Figur darzustellen. Diese Figurenfindung kann immer nur Teilaspekte der Realitat darstellen und nie alle Elemente ausführlich abbilden.

Selbstverständlich geschieht dieser Prozess darüber hinaus in enger Rocksprache mit der Redaktion, der Produktion und vor allem auch dem Fachberater, der die Produktion durch den kompletten Herstellungsprozess berat. Dieser Fachberater, selbst Asperger Autist, ist standig in die Produktion eingebunden, um die Hauptfigur glaubhaft darzustellen. Zahlreiche positive Zuschriften von Zuschauer*innen bestärken uns ebenso darin. All unsere Bemühungen sind geleitet von dem Ziel, das Publikum zu berühren, zu unter halten und im besten Falle zusammenzuführen.

Ich danke Ihnen, sehr geehrter [betalars], für die kritische Begleitung unserer Sendungen. In der Hoffnung, Ihre Bedenken mit meinen Ausführungen ausgeräumt zu haben, würde ich mich freuen, wenn Sie dem ZDF-Programm auch weiterhin als interes sierter und kritischer Zuschauer erhalten bleiben,

Dr. Thomas Bellut

## Antwort von betalars

Sehr geehrte Mitarbeitende der Geschäftsstelle Fernsehrat,

Ich bedanke mich für die mir nun als Teil eines formellen Beschwerdeverfahrens übermittelte Antwort der Programmdirektion auf meine Beschwerde über zwei exemplarische Folgen der Serie "Ella Schön". In dem Schreiben wird erneut die Hoffnung geäußert Bedenken mit Ausführungen ausgeräumt zu haben, dies kann ich jedoch auch diesmal nicht bestätigen. Im Gegenteil: Da die Antwort den Inhalt meiner Programmbeschwerde sachlich falsch darstellt, wurden meine Bedenken weiter verstärkt.

Thomas Bellut schreibt ich hätte bemängelt, dass sich die Serienschaffenden nicht ausführlich genug mit dem Thema Autismus auseinander gesetzt haben und sagt es sei nicht erwartbar, dass die Figur "Ella Schön" alle Elemente der Realität abbilden könne.

Ziel einer formellen Programmbeschwerde ist jedoch die Feststellung, ob konkrete Sendungen gegen die Satzung des ZDF verstoßen. Bemühungen und Intentionen der Produzierenden und Darstellenden haben hier allenfalls zweitrangig Sachbezug.

Unabhängig davon wäre der Anspruch eine Facette menschlicher Diversität allumfassend in einem Werk abzubilden unbestreitbar absurd und wird so weder durch die Satzung noch von meiner Beschwerde eingefordert.

Was ich tatsächlich konkret bemängelt habe ist, dass:
- keine Content Notes bereit gestellt wurden, obwohl sich in beiden Folgen mit vielen Themen auseinander gesetzt wird, die als vergleichsweise häufige Trigger zu werten sind.
- die Folgen darstellen, wie Ella Schön durch einen Unwillen empathisch zu sein die körperliche und mentale Gesundheit anderer gefährdet. In nach Einreichung der Beschwerde statt findender Auseinandersetzung ist darüber hinaus auch deutlich geworden, dass die Figur sogar sehr empathisch sein kann, wenn es ihr zum eigenen Vorteil dient. Beides beschreibt antisoziales Verhalten und hat nichts mit den tatsächlichen Problemen zu tun, die symptomatisch für eine Autismusspektrumsstörung sind: Autist_innen wollen grundsätzlich empathisch sein, können dies aber häufig nicht, weil es ihnen schwer fällt sich in andere hinein zu versetzen.
- die Folgen ableistische Vorurteile gegenüber autistischen Menschen replizieren und so zwar dargestellt wird, wie manche besonders beeinträchtigte vorwiegend männliche Autisten von unserer Gesellschaft aus einer verzerrten neurotypischen Perspektive wahr genommen werden, was jedoch weder den Erfahrungen vieler Autist_innen noch der der objektiven Realität gerecht wird.
- beide Folgen falsch darstellen, wie autistische Eigenschaften in Frauen häufig ausgeprägt sind.
- Masking dargestellt wird ohne zu problematisieren, dass dies eine Schutzreaktion ist und und so symptomatisch für die Marginalisierung, die Autist_innen durch unsere Gesellschaft erleben.
- durch das grundlegende Unverständnis von autistischer Emotionalität in S2E1 eine Szene existiert, die bei mehreren Autist_innen unabhängig voneinander Assoziationen zu sexueller Gewalt hervorgebracht hat.
- Aufgrund dieser Sachargumente habe ich dargelegt, wie die Serie gegen 6 Punkte der ZDF-Satzung verstößt. Nähere Informationen darüber finden Sie auf Seiten 2, 7 und 8 meiner Beschwerde.

Die mir vorliegende Antwort lässt nicht vermuten, dass es eine sachliche Auseinandersetzung mit meinen konkreten Kritikpunkten gegeben hat und ich kann nicht erkennen, dass mein Feedback verstanden wurde. Konkrete Vorschläge, wie dies in Zukunft besser gemacht werden könnte, habe ich ebenfalls nicht erkennen können. Das entspricht nicht meinem Verständnis von einer gesunden Fehlerkultur. Ich habe eher den Eindruck gewonnen das ZDF möchte sich mit allen Mitteln gegen meine wohlwollende Kritik immunisieren - aus Angst einen Fehler einzustehen - und verliert dabei völlig aus den Augen, dass ich es grundsätzlich als eine für unsere Demokratie fundamental wichtige Instution betrachte.

Die Energie und Zeit, die ich in meine Kritik investiere, ist Ausdruck dieser Hochachtung, eine gesunde Fehlerkultur zu sehen wäre für mich eine Bestätigung dieser Anerkennung gewesen. Ich muss nun aber erschrocken feststellen, dass ich das institutionelle Verhalten des ZDF mir gegenüber nicht nachvollziehen kann und dies mich sehr verunsichert.

Ich glaube ich habe hier sehr ausführlich beschrieben, weswegen ich mich mit der Antwort nicht zufrieden geben kann und muss daher einfordern, dass meine Beschwerde vor dem zuständigen Programmausschuss diskutert wird.

Ich stehe dabei nach wie vor für Rückfragen zur Verfügung und habe auch [hier einige wissenschaftliche Quellen bereit gestellt, die meine vorgebrachten Sachargumente untermauern.](https://gitlab.com/betalars/failed-empathy/-/blob/master/weiterf%C3%BChrende_Informationen.md)

mir freundlichen Grüßen
[betalars]

## Disclaimer zu der Rolle des autistischen Beraters:
Das ZDF betont mir gegenüber hier erneut, dass sie sich von einem Asperger-Autisten beraten wurden und so mit diesem eng zusammengearbeitet haben.

**Ich verurteile, dass dieses Argument als Antwort auf eine formelle Programmbeschwerde hervor gebracht wurde.**

Denn nicht nur hat es keinen Sachbezug, es ist auch sowohl mir als auch dem autistischen Berater gegenüber respektlos.

Aus meiner Perspektive impliziert diese Behauptung, dass meine Kritikpunkte nur eingebildet wären und meine 9 Seiten Programmkritik es nicht einmal wert sind gelesen zu werden, weil beim ZDF die Menschen so toll sind, dass sie sogar **einem** Autisten *geredet* haben. Dessen Kompetenz und Erfahrungen müsste ich nun erstmal abstreiten und damit wäre ich ihm gegenüber auch bereits gewaltsam, was meine Kritik weiter delegitimisieren würde.

Gegenüber dem Berater impliziert es, dass er eine Verantwortung dafür gehabt hätte, die von mir kritisierten Verstöße gegen die Satzung abzuwenden.

**Aber die Verantwortung für die Einhaltung der Satzung trägt wenn überhaupt der Intendant!**
Und sich als Reaktion auf Sachkritik auf der Existenz eines Beraters auszuruhen ist grundsätzlich problematisch, zumal ihm auch viele Steine in den Weg gelegt wurden. So weit ich das einschätzen kann war er meistens nicht am Set und hat lediglich die Drehbücher gegengelesen und hat bei der Entwicklung der Figur beratend unterstützt.
Und es ist wirklich keine triviale Aufgabe ist zu extrapolieren, wie Drehbücher durch Regie, Darstellende und Publikum interpretiert werden könnten. Hinzu kommt, dass es ihm als männlicher Autist vermutlich auch schwerer fällt, Erfahrungen autistischer Frauen einzuschätzen.

Dies dann als Entkräftigung meiner Bedenken im Kontext einer formellen Programmbeschwerdevorzubringen ist absurd.
