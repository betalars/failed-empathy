## Antwort des Programmdirektors auf die Programmbeschwerde:

*Dies ist ein Transkript der Antwort des Programmdirektors auf meine Beschwerde an den Rundfunkrat. 
Ursprünglich war ich davon ausgegangen, es handle sich hierbei um eine Stellungnahme gemäß des geregelten [Prozedere eines Beschwerdeverfahrens](https://www.zdf.de/zdfunternehmen/zdf-fernsehrat-foermliche-programmbeschwerde-100.html), nach der ich mein Anliegen an den Rundfunkrat hätte weiter reichen können.*

*Es handelt sich hier allerdings leider einfach nur um eine Antwort aus Kulanz. Mein Verfahren wurde pro Forma abgelehnt ohne, dass ich darüber informiert wurde. Den Text möchte ich dennoch für die Öffentlichkeit dokumentieren.*

*Ich habe meinen Namen unkenntlich gemacht und das mir nur als Pixelgrafik vorliegende Dokument automatisch zu Text konvertiert. Es kann sein, dass dabei einzelne Worte falsch übernommen wurden, ich habe das nicht nochmal Wort für Wort gegengelesen.*

Sehr geehrter Herr [betalars],

In Ihrer Mail vom 19. November 2020 an den Fernsehrat des ZDF haben Sie unsere Sendung „Ella Schön" angesprochen. Die Fernsehratsvorsitzende hat ihre Anfrage an mich zur Prüfung weitergeleitet Gerne möchte ich Ihnen hiermit antworten.

Konkret monieren Sie die Darstellung autistischen Menschen in der betreffenden Sendung.

Nach Rücksprache mit der zuständigen Redaktion kann ich Ihnen versichern, dass die an der Reihe beteiligten Autor\*innen sowie die Produktionsfirma nicht nur intensiv zum Thema Asperger-Autismus recherchiert haben, sondern auch mit autistischen Menschen über dessen durchaus vielfältige Ausprägungen gesprochen haben. Zudem arbeitet die Produktionsfirma bei der Entwicklung der Figur Ella Schon seit Beginn der Reihe mit einem Fachberater zusammen, der selbst Asperger-Autist ist. Dieser gibt den Autor\*innen un verzichtbares Feedback, liest alle Buchfassungen und seine Anmerkungen weden sehr gewissenhaft bedacht sowie umgesetzt.

Wir bedauern es sehr, dass Sie und Menschen in Ihrem Umfeld dennoch von der Darstel lung der Figur „Ella Schön verletzt waren. Wie Sie selbst schreiben, beabsichtigt das an gesprochene Programm zu allerforderst, Vorurteile abzubauen und das Bereichernde eines Miteinanders von autistischen und neurotypischen Menschen zu verdeutlichen - ganz im Sinne unserer Verantwortung als öffentlich-rechtlicher Sender. Ihre Erlauterungen haben eindrücklich ausgeführt, wie relevant fiktive Vorbilder auch für autistische Menschen sind. Daher nehme ich Ihre Kritik ebenso ernst wie den Zuspruch, den wir von an derer Seite für die Reihe erfahren. Ich kann Ihnen versichern, dass beides gleichermaßen in die Weiterentwicklung und Verbesserung der Serie fließen wird. Ihre differenzierten Anmerkungen werden uns helfen, unserem Anspruch noch besser gerecht zu werden.

Ich danke Ihnen, sehr geehrter Herr [betalars], für die kritische Begleitung unserer Sendungen. In der Hoffnung, Ihre Bedenken mit meinen Ausführungen ausgeräumt zu haben, würde ich mich freuen, wenn Sie dem ZDF-Programm auch weiterhin als interessierter und durchaus kritischer Zuschauer erhalten bleiben.

Herzliche Grüße

Dr. Norbert Himmler
