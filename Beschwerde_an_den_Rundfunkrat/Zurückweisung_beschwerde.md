Sehr geehrter [betalars],

nachdem der Beschwerdeausschuss am 20.02.2021 und am 10.06.2021 sowie der Fernsehrat Ihre Beschwerde eingehend beraten haben, darf ich Ihnen gemäß § 21 Absatz 3 der ZDF Satzung zum Ausgang des Beschwerdeverfahrens mitteilen, dass der Fernsehrat Ihre Programmbeschwerde in seiner Sitzung vom 02.07.2021 in Mainz abschließend als unbegründet zurückgewiesen hat. Es wurde kein Verstoß gegen die für das ZDF geltenden Rechtsvorschriften festgestellt.

Der Fernsehrat stellt fest, dass die in der Programmbeschwerde dargelegten persönlichen Verletzungen keine Verletzung von Programmgrundsätzen begründen. Auch lässt sich aus dem Grad individueller Betroffenheit kein Anspruch auf eine bestimmte Zeichnung einer fiktionalen Hauptfigur ableiten, die Besonderheiten von Autismus-Spektrum-Störungen für ein breites Publikum verkörpert Deutlich wird hierbei allerdings, welch hohe Sensibilität bei der Entwicklung einer solchen Figur erforderlich ist, insbesondere zur Vermeidung einer typisierenden Darstellung. Dies endet nicht mit dem Hinzuziehen eines Fachberaters aus dem autistischen Spektrum. Daher anerkennt der Fernsehrat in diesem Fall ausdrücklich, dass die verantwortliche Redaktion hier in einen konstruktiven kommunikativen Austausch mit dem Zuschauer getreten ist.

Auch wenn Ihrer Beschwerde nicht stattgegeben wurde, bleibt eine gut begründete, inhaltlich fundierte Beschwerde im ZDF nicht ohne Wirkung. Die intensive Diskussion mit den Programmverantwortlichen des ZDF, meist in den zuständigen Programmausschüssen, führt zu einem konstruktiven Umgang mit den Inhalten der Beschwerde und, wo nötig, auch zu Reaktionen in der redaktionellen Arbeit.

Mit freundlichen Grüßen

*gezeichnet*  
Marlehn Thieme
