## Über dieses Dokument

Hier werden wissenschaftliche Paper angeführt zu wesentlichen sachlichen Behauptungen dieses Vortrages. Zu jeder Quelle ist auch eine kurze zusammenfassung der wesentlichen These angefügt, die die Quelle zum Vortrag beigetragen hat.

### DMS-5 - Diagnostic and Statistical Manual of Mental Disorders
Aktuelleste Aufgabe der Klassifizierung von allen Mentalen Störungen durch die [American Psychiatric Society](https://www.psychiatry.org/)

 - [Webauftritt des Werks](https://www.psychiatry.org/psychiatrists/practice/dsm)
 - ISBN: 978-0-89042-554-1
 - [Beschreibung auf Wikipedia](https://en.wikipedia.org/wiki/DSM-5)
 - [Zusammenfassung der für Autismus relevanten Diagnosekriterien auf autismus-kultur.de](https://autismus-kultur.de/autismus/dsm-5-diagnosekriterien.html)

### Autismus oder „Psychopathy“? - Literaturübersicht und Kasuistik - Jens Roberz · Gerd Lehmkuhl · Kathrin Sevecke
 - [Publiziert im Springer-Verlag Berlin Heidelberg 2013](https://link.springer.com/content/pdf/10.1007%2Fs11757-013-0232-5.pdf)
 - DOI 10.1007/s11757-013-0232-5
 - wesentliche Thesen:
    - symptomatisch für antisoziale Persönlichkeitsstörung und autismus-Spektrumsstörung sind Probleme in der Empathie
    - diese sind jedoch unterschiedlich:
        - autistische Menschen haben Probleme, sich in die Perspektive andere hineinzuversetzen
        - symptomatisch für antisoziale Persönlichkeitsstörung ist ein gutes Vermögen, andere zu lesen, und trotzdem antisoziales Verhalten
        - sollte beides gleichzeitig auftreten, ist es als komorbid zu betrachten

### Social Camouflaging in Females with Autism Spectrum Disorder - A Systematic Review - María Tubío‐Fungueiriño · Sara Cruz · Adriana Sampaio · Angel Carracedo · Montse Fernández‐Prieto
 - [Publiziert im Springer Science+Business Media, LLC, part of Springer Nature 2020](https://link.springer.com/content/pdf/10.1007/s10803-020-04695-x.pdf)+
 - DOI: 10.1007/s10803-020-04695-x
 - wesentliche Thesen:
     - Autistische Frauen mit durchschnitlicher Intelligenz sind häufig derart gut im verstecken ihrer autistischen Symptome, dass sie oft spät, falsch oder auch gar nicht diagnostiziert werden.
