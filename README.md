# Failed Empathy

Repository mit öffentlichen Informationen über eine Metakritik über Autismusrepräsentation in Medien. Der Vortrag fand auf der Jahresabschlusskonferenz 2020 des CCC auf der Chaos West Bühne statt und kann [hier](https://media.ccc.de/v/rc3-824289-failed_empathy) angeschaut werden.

## Programmbeschwerde über zwei Eipsoden der Serie Ella Schön

Dieses Repository dokumentiert ebenfalls eine förmliche Programmbeschwerde über Episode 1 Season 1 - "die Inselbegabung" und Episode 1 Season 2 - "Die Nackte Wahrheit" der Serie Ella Schön.

Die förmliche Programmbeschwerde wurde am 18. Dezember 2020 durch den Fernsehrat angenommen und wird derzeit noch bearbeitet.
