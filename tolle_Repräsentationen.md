# Tolle Beispiele von Repräsentation

 - der kleine Prinz
     - eine tolle Figur, in der ich viele autistische Wesenszüge erkenne
 - Brand new Animal von Studio Trigger
     - Erzählt von vielen Problemen, die autistische Menschen auch so erleben
     - nutzt dafür eine abstrakte Tiermetapher, die Beastmen haben allerdings eher wenig autistische Wesenszüge
 - Hilda
     - eine weitere Figur, die ich autistisch Lese in einer Serie/Comicreihe, die sich sehr toll mit Diversität beschäftigt.
     - zeigt, wie wertvoll Diversität ist
     - zeigt, dass trotz verschiedener Bedürfnisse miteinander zu leben schwer ist
 - Max und Mary
     - ein sehr düsterer, aber liebevoller Stop-Motion Trickfilm über eine ungewöhnliche Brieffreundschaft eines autistischen Erwachsenen aus New York und einem australsichen Kind
 - Viele Bücher und Comics von [Fuchskind](http://www.fuchskind.de)
 - [tolle comics, von steve asbell](https://www.steveasbell.com/comics)
 - fantastische Tierwesen und wo sie zu finden sind
     - Newt Scemanders ist eine tolle autistishe Figur
     - JKR ist eine furchtbare Person ... leight euch den Film doch in eurer städtischen Bibilothek aus. ;)
 - [das angesprochene Overwatch-Comic](https://playoverwatch.com/en-us/media/stories/a-better-world/)
     - Overwatch verbreitet jedoch auch in der Hintergrundgeschichte einem nach Release hinzugefügten Charakters problematische veraltete Bilder von "Irrenanstalten"
 - [Wintergartan](https://www.youtube.com/user/wintergatan2000) beschäftigt sich auf YouTube im Rahmen der MMX2 sehr toll mit dem Management von großen Projekten.
 - Sence8 ist eine super tolle Serie mit Beispiellos genialer Queer-Representation, die sich sehr intensiv und kritisch mit Marginalisierung auseinander setzt.
 - [Yo Samdy Sam](https://www.youtube.com/watch?v=1hG9709j8fw) beschäftigt sich auf YouTube sehr toll mit verschiedenen Themen rund um Autismus
 - 
 - [skalabyrinth](https://www.karlabyrinth.org) schreibt tolle Bücher über Eutopien und Diversität, liest davon auch Höhrfassungen und veröffentlicht viele Blogartikel über sere (autistischen) Lebenserfahrungen
- Celeste
     - Anspruchsvollers Plattformer-Spiel, dass sich intensiv mit Themen von mentaler Gesundheit, Sozialphobie und Depressionen beschäftigt.
     - [Die Developerin hat auch aufgrund ihrer unabsichtlichen Trans-Repräsentation darüber erfahren, dass sie selbst ebenfalls Trans ist.](https://maddythorson.medium.com/is-madeline-canonically-trans-4277ece02e40)
 - [dieses tolle Spiel auf Newgrounds beschäftigt sich einfühlsam mit Sozialphobie](https://www.newgrounds.com/portal/view/738688)
     - Christine Preissmann - Mit Autismus leben. ISBN 978-3-608-86127-3

## weitere lesenswerte Quellen:
 - [Ein Bericht über die Entstehungsgeschichte von Atypical](https://www.latimes.com/entertainment-arts/tv/story/2019-11-20/netflix-atypical-autism-art)
 - [Wie die autistischne Wesenszüge von Satoshi Tajiri bei der Erstellung von Pokemon halfen](https://the-art-of-autism.com/how-satoshi-tajiris-autism-helped-create-pokemon/)
 - Warum wir auf unsere Öffis aufpassen müssen: [BBC verbietet Mitarbeitenden auch in ihrer Freizeit an Pride Parades teil zu nehmen](https://www.theguardian.com/media/2020/oct/29/bbc-no-bias-rules-prevent-staff-joining-lgbt-pride-protests)
